#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512

# Use infra repository for installation source
url --url=http://10.5.126.23/pub/fedora/linux/releases/18/Fedora/x86_64/os/

# Run the Setup Agent on first boot
firstboot --disable
ignoredisk --only-use=vda

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=eth0 --activate

# Root password
rootpw --iscrypted $6$Qw6Sq./z8VeP.20M$/IZqF9fOxPROo3QbCM05jJ4ZLNvim.6sW69o7izLdkZ//p8bjWqQJpGMHlCbQr.Z2TIH70FFr.iUGPqlMoW931

# System timezone
timezone Etc/UTC --isUtc

# System bootloader configuration
bootloader --location=mbr --boot-drive=vda --append="console=ttyS0"

# Partition clearing information
clearpart --all --initlabel 

# Disk partitioning information
autopart --type=plain

# packages to install
%packages
@core

%end

# make sure the vm shuts down after install instead of waiting for prompt
poweroff

%post

# create the .ssh dir and authorized_keys for root

mkdir -p /root/.ssh
chmod 700 /root/.ssh

# add the infra key
#urlgrabber --output=/root/.ssh/authorized_keys http://infrastructure.phx2.fedoraproject.org/infra/ssh/admin.pub


# add the autoqa keys to root
cat <<EOF >> /root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA2w4s7SdGDR5VpzpTK2GUPreVX7I/DvXYJDVHFutBPfsMNMed1/I4okt/bRB2o8DYbpfRKWNRn5kV7J/n66ZWWyJdGR1QQXNV9p2tWQkTvOui50EPCODipYbsrU78f8U88Gq1FNoycRO8c1UdP/6rttTZYXIlEIcZll84BWf+PEkKI9FMjmwF2zrirZEsJj99fH7p6suPE7W9i3qrHyBeodTVJLZJUDoWgw9Y+H0U64iqOGUl9lOxHHB/5zxCz8Cre8H+MXbqd+1WMEjtlF2+Luqo6H03Xi0ZYgB+3vugEbwiHwxHsVtUhR2lQL3etgIvQc4oij9UaXfng3gVLK5N4w== qa-admin@qa.fedoraproject.org
EOF
%end

