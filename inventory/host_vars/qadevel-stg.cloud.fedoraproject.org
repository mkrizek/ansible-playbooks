---
hostname: qadevel-stg.cloud.fedoraproject.org
deployment_type: qadevel-stg
buildmaster: 172.16.2.10
buildmaster_template: qadevel.master.cfg.j2
# this is a workaround for https://github.com/ansible/ansible/issues/4883
private_vars_file: "/srv/ansible/private/qa/qadevel-stg.yml"

