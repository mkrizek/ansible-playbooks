---
short_hostname: virt02.qa
virthost: qa01.qa.fedoraproject.org
host_macaddr: "52:54:00:fe:22:ff"
fedora_version: 18
fedora_arch: x86_64
lvm_size: 20000
guest_memory: 4096
