# these two lines are required for EL6
import __main__
__main__.__requires__ = ['SQLAlchemy > 0.7', 'jinja2 >= 2.6']
import pkg_resources

from blockerbugs import app as application
